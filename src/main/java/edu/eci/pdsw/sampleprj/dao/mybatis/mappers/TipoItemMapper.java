package edu.eci.pdsw.sampleprj.dao.mybatis.mappers;


import edu.eci.pdsw.samples.entities.TipoItem;
import java.util.List;

/**
 *
 * @author 2106913
 */
public interface TipoItemMapper {
    
    List<TipoItem> getTiposItems();
    
    TipoItem getTipoItem(int id);
    
    void addTipoItem(String des);
}

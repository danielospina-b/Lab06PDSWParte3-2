package edu.eci.pdsw.sampleprj.dao.mybatis.mappers;


import edu.eci.pdsw.samples.entities.Item;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author 2106913
 */
public interface ItemMapper {
    
    
    List<Item> getItems();        
    
    Item consultarItem(@Param("id") int id);
    
    void insertarItem(@Param("id") int id,
            @Param("nombre") String nombre,
            @Param("descripcion") String descripcion,
            @Param("fecha") Date fechaLanz,
            @Param("tarifa") long tarifa,
            @Param("formato") String formato,
            @Param("genero") String genero,
            @Param("tiid") int tiid);

    List<Item> consultarItems();

    List<Item> consultarDisponibles();

        
}

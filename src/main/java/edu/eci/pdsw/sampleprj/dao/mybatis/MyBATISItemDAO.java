/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.pdsw.sampleprj.dao.mybatis;

import com.google.inject.Inject;
import edu.eci.pdsw.sampleprj.dao.ItemDAO;
import edu.eci.pdsw.sampleprj.dao.PersistenceException;
import edu.eci.pdsw.samples.entities.Item;
import edu.eci.pdsw.sampleprj.dao.mybatis.mappers.ItemMapper;
import java.util.List;



/**
 *
 * @author hcadavid
 */
public class MyBATISItemDAO implements ItemDAO{

    @Inject
    private ItemMapper itemMapper;    

    @Override
    public Item load(int id) throws PersistenceException {
        try{
            return itemMapper.consultarItem(id);
        }
        catch(org.apache.ibatis.exceptions.PersistenceException e){
            throw new PersistenceException("Error al consultar el item "+id,e);
        }
        
        
    }

    @Override
    public long consultarTarifaxDia(int iditem) throws PersistenceException {
        try {
            Item item = itemMapper.consultarItem(iditem);
            long res;
            if (item == null) {
                res = 0;
            }
            else {
                res = item.getTarifaxDia();
            }
            return res;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw new PersistenceException("Error al consultar la tarifa por dia", e);
        }
    }

    @Override
    public List<Item> consultarItems() throws PersistenceException {
        try {
            return itemMapper.consultarDisponibles();
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw new PersistenceException("Error al consultar todos los items", e);
        }
        
    }

    @Override
    public void insertarItem(Item i) throws PersistenceException {
        itemMapper.insertarItem(i.getId(), i.getNombre(), i.getDescripcion(), i.getFechaLanzamiento(), i.getTarifaxDia(), i.getFormatoRenta(), i.getGenero(), i.getTipo().getID());
    }
    
}

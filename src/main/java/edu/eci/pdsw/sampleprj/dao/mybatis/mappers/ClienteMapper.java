package edu.eci.pdsw.sampleprj.dao.mybatis.mappers;

import edu.eci.pdsw.sampleprj.dao.PersistenceException;
import edu.eci.pdsw.samples.entities.Cliente;
import edu.eci.pdsw.samples.entities.ItemRentado;
import edu.eci.pdsw.samples.entities.TipoItem;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author 2106913
 */
public interface ClienteMapper {
    
    Cliente consultarCliente(long id); 
    
    /**
     * Registrar un nuevo item rentado asociado al cliente identificado
     * con 'idc' y relacionado con el item identificado con 'idi'
     * @param id id del cliente
     * @param idit id del item
     * @param idir id item rentado
     * @param fechainicio fecha de prestamo
     * @param fechafin fecha final de entrega
     */
    void agregarItemRentadoACliente(@Param("idcli") long id, 
                @Param("iditem") int idit,
                @Param("idir") int idir,
                @Param("finicio") Date fechainicio,
                @Param("ffin") Date fechafin);

    /**
     * Consultar todos los clientes
     * @return Lista de clientes
     */
    List<Cliente> consultarClientes();
    
    void registrarCliente(@Param("docu") long documento,
            @Param("nombre") String nombre,
            @Param("tel") String telefono,
            @Param("dir") String direccion,
            @Param("email") String email);
    
    ItemRentado getItemRentado(@Param("id") int idCliente) throws PersistenceException;

    void registrarAlquiler();

    int getNextValue();
    
    TipoItem consultarTipoItem(@Param("id") int id) throws PersistenceException;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.pdsw.sampleprj.dao.mybatis;

import com.google.inject.Inject;
import edu.eci.pdsw.sampleprj.dao.ClienteDAO;
import edu.eci.pdsw.sampleprj.dao.PersistenceException;
import edu.eci.pdsw.sampleprj.dao.mybatis.mappers.ClienteMapper;
import edu.eci.pdsw.samples.entities.Cliente;
import edu.eci.pdsw.samples.entities.ItemRentado;
import edu.eci.pdsw.samples.entities.TipoItem;
import java.sql.Date;
import java.util.List;



/**
 *
 * @author hcadavid
 */
public class MyBATISClienteDAO implements ClienteDAO {
    
    @Inject
    private ClienteMapper clienteMapper; 
    
    @Override
    public Cliente load(long id) throws PersistenceException {
        return clienteMapper.consultarCliente(id);
    }

    @Override
    public void save(Cliente c) throws PersistenceException {
        clienteMapper.registrarCliente(c.getDocumento(), c.getNombre(), c.getTelefono(), c.getDireccion(), c.getEmail());
    }

    @Override
    public List<Cliente> consultarClientes() throws PersistenceException {
        return clienteMapper.consultarClientes();
    }

    @Override
    public ItemRentado getItemRentado(int iditem) throws PersistenceException {
        return clienteMapper.getItemRentado(iditem);
    }

    @Override
    public void registrarAlquiler(long docu, int id, Date fechainicio, Date fechafin) throws PersistenceException {
        int idir = clienteMapper.getNextValue();
        clienteMapper.agregarItemRentadoACliente(docu, id, idir, fechainicio, fechafin);
    }

    @Override
    public TipoItem consultarTipoItem(int id) throws PersistenceException {
        return clienteMapper.consultarTipoItem(id);
    }
        
}

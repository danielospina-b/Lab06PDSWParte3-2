/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.pdsw.samples.tests;

import edu.eci.pdsw.samples.entities.Cliente;
import edu.eci.pdsw.samples.services.ExcepcionServiciosAlquiler;
import edu.eci.pdsw.samples.services.ServiciosAlquiler;
import edu.eci.pdsw.samples.services.ServiciosAlquilerFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.exceptions.PersistenceException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * CE1: Se debe agregar un cliente  con todos los datos completos normalmente. TIPO: Normal. 
 * Resultado Esperado: El cliente existe en la lista de clientes
 * 
 * CE2: Se debe agregar un cliente con datos en sus campos = null. TIPO: Frontera.
 * Resultado Esperado: El objeto cliente sebe estar en la lista de clientes
 * 
 */
public class ClientesTest {

    public ClientesTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @Test
    public void testCE1() {
        ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();
        Cliente cliente = new Cliente("Juan Camilo Ramirez", 1015354786, "3214896", "Cll73#45-21", "juancramirez@yopmail.com");
        try {
            sa.registrarCliente(cliente);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("No se pudo ingresar el cliente, lanzo excepcion");
        }
        Cliente res = null;
        try {
            res = sa.consultarCliente(1015354786);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Fallo al consultar el cliente");
        }
        assertTrue("Comprobando clientes iguales", cliente.toString().equals(res.toString()));
    }   
    
    @Test
    public void testCE2() {
        try {
            ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();
            long id = 1;
            int tamanoInicial = sa.consultarClientes().size();
            Cliente cliente = new Cliente(null, id, null, null, null);
            sa.registrarCliente(cliente);
            assertTrue("Se agregó el cliente a la lista", sa.consultarClientes().size() == tamanoInicial + 1);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("No se pudo agregar el cliente nulo");
        }
    }

    /**
     * CE3: Agregar dos clientes con el mismo documento
     *      TIPO: Frontera. Resultado Esperado: No se agrega
     */ 
    @Test
    public void testCE3() {
        ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();
        try {
            
            Cliente cliente1 = new Cliente("A", 1, "1", "A", "A");
            Cliente cliente2 = new Cliente("A", 1, "1", "A", "A");
            sa.registrarCliente(cliente1);
            sa.registrarCliente(cliente2);
            assertTrue("Cliente ya ingresado" + sa.consultarClientes().size(), sa.consultarClientes().size() == 1);
        } catch (ExcepcionServiciosAlquiler | PersistenceException ex) {
            //Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            
        }
    }   
}

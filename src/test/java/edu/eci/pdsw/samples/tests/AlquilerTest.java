/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.pdsw.samples.tests;

import edu.eci.pdsw.samples.entities.Cliente;
import edu.eci.pdsw.samples.entities.Item;
import edu.eci.pdsw.samples.entities.ItemRentado;
import edu.eci.pdsw.samples.entities.TipoItem;
import edu.eci.pdsw.samples.services.ExcepcionServiciosAlquiler;
import edu.eci.pdsw.samples.services.ServiciosAlquiler;
import edu.eci.pdsw.samples.services.ServiciosAlquilerFactory;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;



/**
 * 
 * Calculo Multa:
 * 
 * Frontera:
 * CF1: Multas a devoluciones hechas en la fecha exacta (multa 0).
 * 
 * Clases de equivalencia:
 * CE1: Multas hechas a devoluciones realizadas en fechas posteriores
 * a la limite. (multa multa_diaria*dias_retraso)
 * CE2: El item despues de prestado no debe estar disponible, y debe estar asignado al cliente
 * CE3: La multa debe ser cero si el item se entrega antes de la fecha limite
 * 
 */
public class AlquilerTest {

    public AlquilerTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @Test
    public void CF1Test() throws ExcepcionServiciosAlquiler{
        ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();
        
        Item i1=new Item(new TipoItem(0, "Pelicula"), 44, "Los 4 Fantasticos", "Los 4 Fantásticos  es una película de superhéroes  basada en la serie de cómic homónima de Marvel.", java.sql.Date.valueOf("2005-06-08"), 2000, "DVD", "Ciencia Ficcion");        
        sa.registrarCliente(new Cliente("Juan Perez",3842,"24234","calle 123","aa@gmail.com"));
        sa.registrarItem(i1);
                
        Item item=sa.consultarItem(44);
        
        sa.registrarAlquilerCliente(java.sql.Date.valueOf("2005-12-20"), 3842, item, 5);
        
        assertEquals("Se calcula correctamente la multa (0) "
                + "cuando la devolucion se realiza el dia limite."
                ,0, sa.consultarMultaAlquiler(sa.consultarItemsCliente(3842).get(0).getId(), java.sql.Date.valueOf("2005-12-25")));
                
    }
    

    @Test
    public void CE1Test() throws ExcepcionServiciosAlquiler{
        ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();

        Item i1=new Item(new TipoItem(0, "Pelicula"), 55, "Los 4 Fantasticos", "Los 4 Fantásticos  es una película de superhéroes  basada en la serie de cómic homónima de Marvel.", java.sql.Date.valueOf("2005-06-08"), 2000, "DVD", "Ciencia Ficcion");        
        sa.registrarCliente(new Cliente("Juan Perez",9843,"24234","calle 123","aa@gmail.com"));
        sa.registrarItem(i1);
                
        Item item=sa.consultarItem(55);
        
        sa.registrarAlquilerCliente(java.sql.Date.valueOf("2005-12-20"), 9843, item, 3);
        //prueba: 3 dias de retraso
        assertEquals("Se calcula correctamente la multa cuando la devolucion se realiza varios dias despues del limite.",
                sa.valorMultaRetrasoxDia()*3,sa.consultarMultaAlquiler(sa.consultarItemsCliente(9843).get(0).getId(), java.sql.Date.valueOf("2005-12-26")));
                
    }    
    
    @Test
    public void CE2Test() throws ExcepcionServiciosAlquiler {
        ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();
        Cliente cliente = new Cliente("Ramiro", 0, "123", "123", "21123");
        sa.registrarCliente(cliente);
        Item item = new Item(new TipoItem(0, "PruebaTipoItem"), 9938, "Prueba", "Prueba Descripcion", java.sql.Date.valueOf("2005-12-28"), 3, "DVD", "Prueba Genero");
        sa.registrarItem(item);
        sa.registrarAlquilerCliente(java.sql.Date.valueOf("2005-12-20"), 0, item, 2);
        try {
            assertTrue("Consultar si el item esta disponible", sa.consultarItemsDisponibles().size() == 0);
        } catch (ExcepcionServiciosAlquiler e) {
            
        }
        List<ItemRentado> lista = sa.consultarItemsCliente(0);
        assertTrue("El item rentado es el mismo item que se registro", lista.get(0).getItem().toString().equals(item.toString()));
    }
    
    @Test
    public void CE3Test() {
            ServiciosAlquiler sa=ServiciosAlquilerFactory.getInstance().getServiciosAlquilerTesting();
            Cliente cliente = new Cliente("Prueba", 5678, "314234", "1234123", "2341234");
        try {
            sa.registrarCliente(cliente);
        } catch (ExcepcionServiciosAlquiler ex) {
            fail("Error al registrarCliente");
        }
            Item item = new Item(new TipoItem(0, "Prueba TipoItem"), 9938, "Prueba", "Prueba Descripcion", java.sql.Date.valueOf("2007-12-28"), 3, "DVD", "Prueba Genero");
        try {
            sa.registrarItem(item);
        } catch (ExcepcionServiciosAlquiler ex) {
            fail("Error al registrarItem");
        }
        try {
            sa.registrarAlquilerCliente(java.sql.Date.valueOf("2017-03-03"), 5678, item, 10);
        } catch (ExcepcionServiciosAlquiler ex) {
            fail("Error al registrarAlquilerCliente");
        }
        try {
            assertEquals("Comprobando que la multa es 0", 0, sa.consultarMultaAlquiler(sa.consultarItemsCliente(5678).get(0).getId(), java.sql.Date.valueOf("2017-03-10")));
        } catch (ExcepcionServiciosAlquiler ex) {
            fail("Error al comprobar multa == 0 usando consultarMultaAlquiler  -  " + ex.getMessage());
        }
        
        
    }
}
